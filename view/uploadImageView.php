<div id="corps">
    <div class="accordion" id="uploadAccordion">
    <div class="card">
        <div class="card-header" id="headingOne">
        <h5 class="mb-0">
            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
            Héberger une image
            </button>
        </h5>
        </div>

        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#uploadAccordion">
        <div class="card-body">
            <form action="index.php?controller=photo&action=upload" method="post" enctype="multipart/form-data">
                <div class="custom-file">
                    <input type="file" class="custom-file-input" name="imageToUpload" id="imageToUpload" required>
                    <label class="custom-file-label" for="imageToUpload">Choisir une image</label>
                </div>
                <div class="form-group">
                    <label for="category">Catégorie</label>
                    <input type="text" class="form-control" name="category" id="category" placeholder="Catégorie" required>
                </div>
                <div class="form-group">
                    <label for="comment">Commentaire</label>
                    <textarea class="form-control" name="comment" id="comment" placeholder="Commentaire"></textarea>
                </div>
                <input type="submit" class="btn btn-outline-success" value="Upload image" name="submit">
            </form>
        </div>
        </div>
    </div>
    <div class="card">
        <div class="card-header" id="headingTwo">
        <h5 class="mb-0">
            <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
            Ajouter une image à partir d'une URL
            </button>
        </h5>
        </div>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#uploadAccordion">
        <div class="card-body">
            <form action="index.php?controller=photo&action=add" method="post">
                <div class="form-group">
                    <label for="url">URL de l'image</label>
                    <input type="text" class="form-control" name="url" id="url" placeholder="URL" required>
                </div>
                <div class="form-group">
                    <label for="category">Catégorie</label>
                    <input type="text" class="form-control" name="category" id="category" placeholder="Catégorie" required>
                </div>
                <div class="form-group">
                    <label for="comment">Commentaire</label>
                    <textarea class="form-control" name="comment" id="comment" placeholder="Commentaire"></textarea>
                </div>
                <input type="submit" class="btn btn-outline-success" value="Add image" name="submit">
            </form>
        </div>
        </div>
    </div>
    </div>
</div>
