<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"  xml:lang="fr" >
	<head>
		<title>Site SIL3</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link rel="stylesheet" type="text/css" href="assets/style.css" media="screen" title="Normal" />
		<link rel="stylesheet" type="text/css" href="assets/bootstrap.min.css" media="screen" />
		<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
	</head>
	<body>
		<div class="container-fluid text-center">
			<ul class="list-group">
				<li class="list-group-item"><h1>Site SIL3</h1></li>
			</ul>
			<div id="menu">		
				<h3>Menu</h3>
				<div class="list-group">
					<?php # Mise en place du menu par un parcours de la table associative
						foreach ($data->menu as $item => $act) {
							print "<a class=\"list-group-item list-group-item-action\" href=\"$act\">$item</a>\n";
						}
						?>
				</div>
			</div>
			<div>
				<?php
					include($data->content);
				?>
			</div>
			<div id="pied_de_page"></div>
		</div>	   	   	
		</body>
	</html>




