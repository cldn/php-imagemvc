<h1>Sélectionner une catégorie</h1>
<form action="index.php?controller=photo&action=first" method="post">
    <select name="category" class="custom-select" required>
        <?php
        global $data;
        foreach($data->categories as $categorie) {
            $categorie = $categorie["category"];
            print("<option value=\"$categorie\">$categorie</option>");
        }
        ?>
    </select>
    <input class="btn btn-outline-success" type="submit" value="Filtrer" name="submit">
</form>
