<?php
	require_once("image.php");
	# Le 'Data Access Object' d'un ensemble images
	class ImageDAO {
		# Chemin LOCAL où se trouvent les images
		private $path="model/IMG";
		# Attribut de connection à la base de donnée
		private $db;
		# Chemin URL où se trouvent les images
		const urlPath="http://localhost/sites/image/model/IMG";
		
		# Tableau pour stocker tous les chemins des images
		private $imgEntry;
		
		# Lecture récursive d'un répertoire d'images
		# Ce ne sont pas des objets qui sont stockes mais juste
		# des chemins vers les images.
		private function readDir($dir) {
			# build the full path using location of the image base
			$fdir=$this->path.$dir;
			if (is_dir($fdir)) {
				$d = opendir($fdir);
				while (($file = readdir($d)) !== false) {
					if (is_dir($fdir."/".$file)) {
						# This entry is a directory, just have to avoid . and .. or anything starts with '.'
						if (($file[0] != '.')) {
							# a recursive call
							$this->readDir($dir."/".$file);
						}
					} else {
						# a simple file, store it in the file list
						if (($file[0] != '.')) {
							$this->imgEntry[]="$dir/$file";
						}
					}
				}
			}
		}
		
	
		
		function __construct($imgpath = "model/IMG") {
			$dsn = "sqlite:".__DIR__."/imageDB"; // Data source name
			$user= ''; // Utilisateur
			$pass= ''; // Mot de passe
			try {
				$this->db = new PDO($dsn, $user, $pass); //$db est un attribut privé d'ImageDAO
				$this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			} 
			catch (PDOException $e) {
				die ("Erreur : ".$e->getMessage());
			}
		}
		
		# Retourne le nombre d'images référencées dans le DAO
		function size() {
			$s = $this->db->query('SELECT * FROM image');
			if ($s) {
				$size = count($s->fetchAll(PDO::FETCH_ASSOC));
				return $size;
			} else {
				print "Error in getImage <br/>";
				$err= $this->db->errorInfo();
				print $err[2]."<br/>";
			}
		}
		
		# Ajouter une image à l'aide d'un chemin, d'une catégorie et d'un commentaire
		function addImage($path, $category, $comment) {
			$id = $this->size() + 1;
			$query = "INSERT INTO image VALUES ('$id', '$path', '$category', '$comment')";
			$s = $this->db->query($query);
			if ($s) {
				return $id;
			} else {
				echo("addImage call failed");
				echo("<br/>");
			}
		}

		# Mettre à jour la catégorie et le commentaire d'une image à partir de son ID
		function updateImage($id, $category, $comment) {
			$s = $this->db->query("UPDATE image SET comment='$comment', category='$category' WHERE id=$id");
			if ($s) {
				return true;
			} else {
				echo ("updateImage call failed");
				echo("<br/>");
			}
		}

		function getImage($id) {
			$s = $this->db->query('SELECT * FROM image WHERE id='.$id);
			if ($s) {
				$result = $s->fetchObject();
				return new Image($result->path, $result->id, $result->category, $result->comment);
			} else {
				print "Error in getImage. id=".$id."<br/>";
				$err= $this->db->errorInfo();
				print $err[2]."<br/>";
			}
		}		
		
		# Retourne une image au hazard
		function getRandomImage() {
			$imgId = rand(1, $this->size());
			return $this->getImage($imgId);		
		}
		
		# Retourne l'objet de la premiere image
		function getFirstImage() {
			return $this->getImage(1);
		}
		
		# Retourne l'image suivante d'une image
		function getNextImage(image $img) {
			$id = $img->getId();
			if ($id < $this->size()) {
				$img = $this->getImage($id+1);
			}
			return $img;
		}
		
		# Retourne l'image précédente d'une image
		function getPrevImage(image $img) {
			$id = $img->getId();
			if ($id > 1) {
				$img = $this->getImage($id-1);
			}
			return $img;
		}
		
		# saute en avant ou en arrière de $nb images
		# Retourne la nouvelle image
		function jumpToImage(image $img,$nb) {
			return $this->getImage($img->getId() + $nb);
		}
		
		# Retourne la liste des images consécutives à partir d'une image
		function getImageList(image $img,$nb) {
			# Verifie que le nombre d'image est non nul
			if (!$nb > 0) {
				debug_print_backtrace();
				trigger_error("Erreur dans ImageDAO.getImageList: nombre d'images nul");
			}
			$id = $img->getId();
			$max = $id+$nb;
			while ($id < $this->size() && $id < $max) {
				$res[] = $this->getImage($id);
				$id++;
			}
			return $res;
		}

		# Return the list of the existing categories
		function getCategories() {
			$s = $this->db->query("SELECT category FROM IMAGE GROUP BY category");
			if ($s) {
				$result = $s->fetchAll();
				if ($result == false) {
					die("Aucune catégorie trouvée");
				} else {
					return $result;
				}
			} else {
				die("Could not get the categories");
			}
		}

		# Return the first image of the category in parameter
		function getFirstFilteredImage($category) {
			$s = $this->db->query("SELECT * FROM image WHERE category='$category'");
			if ($s) {
				$result = $s->fetchObject();
				if ($result == false) {
					die("Aucune image trouvée pour la catégorie ".$category);
				} else {
					return new Image($result->path, $result->id, $result->category, $result->comment);
				}
			} else {
				die("Could not get first filtered image");
			}
		}
		
		# Return the successor of the image which has the ID and the category in parameter
		function getNextFilteredImage($imgId, $category) {
			$s = $this->db->query("SELECT * FROM image WHERE id>$imgId AND category='$category'");
			if ($s) {
				$result = $s->fetchObject();
				if ($result == false) {
					// The request returned 0 results
					return $this->getImage($imgId);
				} else {
					// A successor was found
					return new Image($result->path, $result->id, $result->category, $result->comment);
				}
			} else {
				die("Could not get next filtered image");
			}
		}
		
		# Return the predecessor of the image which has the ID and the category in parameter
		function getPrevFilteredImage($imgId, $category) {
			$s = $this->db->query("SELECT * FROM image WHERE id<$imgId AND category='$category'");
			if ($s) {
				$result = $s->fetchAll();
				if ($result == false) {
					// The request returned 0 results
					return $this->getImage($imgId);
				} else {
					// A predecessor was found. It is the last element of the array (which means it is the closest to $imgId)
					$result = end($result);
					return new Image($result['path'], $result['id'], $result['category'], $result['comment']);
				}
			} else {
				die("Could not get prev filtered image");
			}
		}

		# Return the whole image list for a category
		function getCategoryImageList($category) {
			$s = $this->db->query("SELECT * FROM image WHERE category='$category'");
			if ($s) {
				$result = $s->fetchAll();
				foreach ($result as $img) {
					$imgList[] = new Image($img['path'], $img['id'], $img['category'], $img['comment']);
				}
				return $imgList;
			} else {
				die("Could not get category image list");
			}
		}

		# Return a random image from a given category
		function getRandomFilteredImage($category) {
			$imgList = $this->getCategoryImageList($category);
			$imgIndex = rand(0, count($imgList) - 1);
			return $imgList[$imgIndex];
		}

		# Return the list of images matching the category in parameter
		function getFilteredImageList($category, $img, $nbImg) {
			if (!$nbImg > 0) {
				debug_print_backtrace();
				trigger_error("Erreur dans ImageDAO.getImageList: nombre d'images nul");
			}
			$id = $img->getId();
			$s = $this->db->query("SELECT * FROM image WHERE id>=$id AND category='$category'");
			if ($s) {
				$result = $s->fetchAll();
				# If there are more results than expected images, only take #nbImg Images
				if (count($result) > $nbImg) {
					for ($i = 0; $i < $nbImg; $i++) {
						$imgList[] = new Image($result[$i]['path'], $result[$i]['id'], $result[$i]['category'], $result[$i]['comment']);
					}
				# If it's not the case, then there are more expected images than results, we can just return the results
				} else {
					foreach ($result as $img) {
						$imgList[] = new Image($img['path'], $img['id'], $img['category'], $img['comment']);
					}
				}
				return $imgList;
			} else {
				die("Could not get filtered image list");
			}
		}
	}
	
	# Test unitaire
	# Appeler le code PHP depuis le navigateur avec la variable test
	# Exemple : http://localhost/sites/image/model/imageDAO.php?test
	if (isset($_GET["test"])) {
		echo "<H1>Test de la classe ImageDAO</H1>";
		$imgDAO = new ImageDAO();
		echo "<p>Creation de l'objet ImageDAO.</p>\n";
		echo "<p>La base contient ".$imgDAO->size()." images.</p>\n";
		$img = $imgDAO->getFirstImage("");
		echo "<p>La premiere image est : ".$img->getURL()."</p>\n";
		# Affiche l'image
		echo "<img src=\"".$img->getURL()."\"/>\n";
		$jumpImg = $imgDAO->jumpToImage($img, 100);
		echo "<p>L'image 100 positions plus loin est : ".$jumpImg->getURL()."</p>\n";
		echo "<img src=\"".$jumpImg->getURL()."\"/>\n";
		$prevImg = $imgDAO->getPrevImage($jumpImg);
		echo "<p>L'image précédente étant : ".$prevImg->getURL()."</p>\n";
		echo "<img src=\"".$prevImg->getURL()."\"/>\n";
		$nextImg = $imgDAO->getNextImage($jumpImg);
		echo "<p>L'image suivante étant : ".$nextImg->getURL()."</p>\n";
		echo "<img src=\"".$nextImg->getURL()."\"/>\n";
		$randImg = $imgDAO->getRandomImage();
		echo "<p>Une image aléatoire : ".$randImg->getURL()."</p>\n";
		echo "<img src=\"".$randImg->getURL()."\"/>\n";
	}
	
	
	?>