<div id="corps">
    <?php
        $imgId = $data->firstId;
		$imgCount = count($data->imgMatrix);
		$size = $data->size;

		# Mise en place des deux boutons
		print "<p>\n";
		print "<div class=\"btn-group\" role=\"group\">";
		if (property_exists($data, "filter") && $data->filter == true) {
			$categorie = $data->categorie;
			print "<a href=\"index.php?controller=photoMatrix&action=prev&size=$data->size&imgId=$imgId&nbImg=$imgCount&category=$categorie\" class=\"btn btn-outline-primary\">Prev</a>\n";
			print "<a href=\"index.php?controller=photoMatrix&action=next&size=$data->size&imgId=$imgId&nbImg=$imgCount&category=$categorie\" class=\"btn btn-outline-primary\">Next</a>\n";
		} else {
			print "<a href=\"index.php?controller=photoMatrix&action=prev&size=$data->size&imgId=$imgId&nbImg=$imgCount\" class=\"btn btn-outline-primary\">Prev</a>\n";
			print "<a href=\"index.php?controller=photoMatrix&action=next&size=$data->size&imgId=$imgId&nbImg=$imgCount\" class=\"btn btn-outline-primary\">Next</a>\n";
		}
		print "</div>";
		print "</p>\n";
        foreach ($data->imgMatrix as $i) {
			print "<a href=\"".$i[1]."\"><img src=\"".$i[0]."\" width=\"".$size."\" height=\"".$size."\"></a>\n";
		};
    ?>
</div>