<?php
    require_once("model/imageDAO.php");

    class Photo {

        private $imageDAO;
        
        function __construct() {
            $this->imageDAO = new ImageDAO();
        }

        public function index() {
            $this->first();
        }

        public function upload() {
            $targetDir = "jons/uploads/";
            $targetFile = $targetDir . basename($_FILES["imageToUpload"]["name"]);
            $uploadOk = 1;
            $imageFileType = strtolower(pathinfo($targetFile,PATHINFO_EXTENSION));
            // Check that the input file is a valid image
            if(isset($_POST["submit"])) {
                $check = getimagesize($_FILES["imageToUpload"]["tmp_name"]);
                if($check !== false) {
                    $uploadOk = 1;
                } else {
                    echo "Le fichier en entrée n'est pas une image.";
                    $uploadOk = 0;
                }
            }

            // Make sure the file doesn't already exist
            if (file_exists($targetFile)) {
                echo "Cette image existe déjà.";
                $uploadOk = 0;
            }
            // Check file size
            if ($_FILES["imageToUpload"]["size"] > 500000) {
                echo "Ce fichier est trop large, veuillez le compresser.";
                $uploadOk = 0;
            }
            // Check file type
            if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
            && $imageFileType != "gif" ) {
                echo "Les formats autorisés sont JPG, JPEG, PNG, GIF.";
                $uploadOk = 0;
            }
            // Check if $uploadOk is set to 0 by an error
            if ($uploadOk == 0) {
                echo "Echec du téléversement.";
            // if everything is ok, try to upload file
            } else {
                if (move_uploaded_file($_FILES["imageToUpload"]["tmp_name"], $targetFile)) {
                    $newImageId = $this->imageDAO->addImage($targetFile, $_POST["category"], $_POST["comment"]);
                    $img = $this->imageDAO->getImage($newImageId);
                    global $data;
                    $this->prepareView("uploadSuccessView.php", NULL, $img, 480);
                } else {
                    echo "Désolé, une erreur est survenue.";
                }
            }
        }

        public function add() {
            if(isset($_POST["submit"])) {
                $newImageId = $this->imageDAO->addImage($_POST["url"], $_POST["category"], $_POST["comment"]);
                $img = $this->imageDAO->getImage($newImageId);
                global $data;
                $this->prepareView("uploadSuccessView.php", NULL, $img, 480);
            }
        }

        public function view() {
            global $imgId, $size, $category;
            getParam();
            global $data;
            $img = $this->imageDAO->getImage($imgId);
            $this->prepareView("photoView.php", $category, $img, $size);
        }

        public function update() {
            global $imgId;
            getParam();
            if (isset($_POST["category"]) && isset($_POST["comment"])) {
                global $data;
                $updatedCategory = $_POST["category"];
                $updatedComment = $_POST["comment"];
                $this->imageDAO->updateImage($imgId, $updatedCategory, $updatedComment);
                echo("L'image a bien été mise à jour");
                $this->view();
            }
        }

        public function first() {
            global $size, $category;
            getParam();
            if (isset($_POST["submit"])) {
                $category = $_POST["category"];
            }
            global $data;
            if ($category != NULL) {
                $img = $this->imageDAO->getFirstFilteredImage($category);
            } else {
                $img = $this->imageDAO->getImage(1);
            }
            $this->prepareView("photoView.php", $category, $img, $size);
        }

        public function next() {
            global $imgId, $size, $category;
            getParam();
            global $data;
            // Get the current image, and then call its successor method
            if ($category != NULL) {
                $img = $this->imageDAO->getNextFilteredImage($imgId, $category);
            } else {
                $img = $this->imageDAO->getImage($imgId); 
                $img = $this->imageDAO->getNextImage($img);
            }
            $this->prepareView("photoView.php", $category, $img, $size);
        }

        public function prev() {
            global $imgId, $size, $category;
            getParam();
            global $data;
            // Get the current image, and then call its predecessor method
            if ($category != NULL) {
                $img = $this->imageDAO->getPrevFilteredImage($imgId, $category);
            } else {
                $img = $this->imageDAO->getImage($imgId); 
                $img = $this->imageDAO->getPrevImage($img);
            }
            $this->prepareView("photoView.php", $category, $img, $size);
        }

        public function random() {
            global $size, $category;
            getParam();
            global $data;
            if ($category != NULL) {
                $img = $this->imageDAO->getRandomFilteredImage($category);
            } else {
                $img = $this->imageDAO->getRandomImage();
            }
            $this->prepareView("photoView.php", $category, $img, $size);
        }

        public function zoomin() {
            global $imgId, $size, $category;
            getParam();
            $size *= 1.25;
            $img = $this->imageDAO->getImage($imgId); 
            $this->prepareView("photoView.php", $category, $img, $size);
        }

        public function zoomout() {
            global $imgId, $size, $category;
            getParam();
            $size *= 0.8;
            $img = $this->imageDAO->getImage($imgId);
            $this->prepareView("photoView.php", $category, $img, $size);
        }

        private function prepareView($viewName, $category, $img, $size) {
            // Prepare the object to give to the view
            global $data;
            $data = new stdClass();
            if ($category) {
                $data->filter = true;
            }
            $data->content = $viewName;
            $data->image = $img;
            $data->size = $size;
            $data->menu = photoMenu($img->getId(), $size, $category);
            require_once("view/mainView.php");
        }
    }

    // Get the current parameters : ID and size of the image
    function getParam() {
        global $imgId, $size, $category;
        if (isset($_GET['imgId'])) {
            $imgId = $_GET['imgId'];
        }
        if (isset($_GET['size'])) {
            $size = $_GET['size'];
        } else {
            $size = 480;
        }
        if (isset($_GET['category'])) {
            $category = $_GET['category'];
        } else {
            $category = NULL;
        }
    }

    function photoMenu($imgId, $size, $category) {
        return $category ?
        array(
            "Home" => "index.php",
            'A propos' => "index.php?controller=home&action=aPropos",
            'Upload' => "index.php?controller=home&action=uploadImage",
            'First' => "index.php?controller=photo&action=first&category=$category",
            'Random' => "index.php?controller=photo&action=random&category=$category",
            'Filter' => "index.php?controller=home&action=filter",
            'More' => "index.php?controller=photoMatrix&action=more&size=$size&imgId=$imgId&nbImg=1&category=$category",
            'Zoom +' => "index.php?controller=photo&action=zoomin&size=$size&imgId=$imgId&category=$category",
            'Zoom -' => "index.php?controller=photo&action=zoomout&size=$size&imgId=$imgId&category=$category"
        ) :
        array(
            "Home" => "index.php",
            'A propos' => "index.php?controller=home&action=aPropos",
            'Upload' => "index.php?controller=home&action=uploadImage",
            'First' => "index.php?controller=photo&action=first&size=$size",
            'Random' => "index.php?controller=photo&action=random&size=$size",
            'Filter' => "index.php?controller=home&action=filter",
            'More' => "index.php?controller=photoMatrix&action=more&size=$size&imgId=$imgId&nbImg=1",
            'Zoom +' => "index.php?controller=photo&action=zoomin&size=$size&imgId=$imgId",
            'Zoom -' => "index.php?controller=photo&action=zoomout&size=$size&imgId=$imgId"
        );
    }

?>