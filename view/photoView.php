<div id="corps">
    <?php
        $url = $data->image->getURL();
		$id = $data->image->getId();
		$categorie = $data->image->getCategory();
		$comment = $data->image->getComment();

		# Mise en place des boutons
		print "<p>\n";
		print "<div class=\"btn-group\" role=\"group\">";
		if (property_exists($data, "filter") && $data->filter == true) {
			print "<a href=\"index.php?controller=photo&action=prev&size=$data->size&imgId=$id&category=$categorie\" class=\"btn btn-outline-primary\">Prev</a>\n";
			print "<a href=\"index.php?controller=photo&action=next&size=$data->size&imgId=$id&category=$categorie\" class=\"btn btn-outline-primary\">Next</a>\n";
		} else {
			print "<a href=\"index.php?controller=photo&action=prev&size=$data->size&imgId=$id\" class=\"btn btn-outline-primary\">Prev</a>\n";
			print "<a href=\"index.php?controller=photo&action=next&size=$data->size&imgId=$id\" class=\"btn btn-outline-primary\">Next</a>\n";
		}
		print "</div>\n";
		print "<button type=\"button\" class=\"btn btn-outline-info\" data-toggle=\"modal\" data-target=\"#updateFormModal\">Mettre à jour l'image</button>";
		print "</p>\n";
		# Métadonnées de l'image
		print"<p>Catégorie : $categorie</p>\n";
		print"<p>Commentaire : $comment</p>\n";
		# Affiche l'image avec une réaction au clic
		print "<a href=\"index.php?controller=photo&action=zoomin&size=$data->size&imgId=$id\">\n";
		# Réalise l'affichage de l'image
		print "<img src=\"$url\" width=\"$data->size\">\n";
		print "</a>\n";
	?>
</div>

	

<!-- Update modal -->
<div class="modal fade" id="updateFormModal" tabindex="-1" role="dialog" aria-labelledby="updateFormModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="updateFormModalLabel">Mettre à jour les informations</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="index.php?controller=photo&action=update&imgId=<?php print $id ?>" method="post">
        <div class="modal-body">
          <div class="form-group">
            <label for="category">Catégorie</label>
            <input type="text" class="form-control" name="category" id="category" placeholder="Catégorie" value="<?php print $categorie ?>" required>
          </div>
          <div class="form-group">
            <label for="comment">Commentaire</label>
            <textarea class="form-control" name="comment" id="comment" placeholder="Commentaire"><?php print $comment ?></textarea>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Fermer</button>
          <button type="submit" class="btn btn-outline-success" value="Update image">Enregistrer</button>
        </div>
      </form>
    </div>
  </div>
</div>
