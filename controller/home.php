<?php
    require_once("model/imageDAO.php");
	
	class Home {

        private $imageDAO;
        
        function __construct() {
            $this->imageDAO = new ImageDAO();
        }
        
        public function index() {
            loadView("homeView.php");
        }
        
        public function apropos() {
            loadView("aproposView.php");
        }

        public function uploadImage() {
            loadView("uploadImageView.php");
        }

        public function filter() {
            global $data;
            $categories = $this->imageDAO->getCategories();
            loadFilterView($categories);
        }

    }

    function loadView($viewName) {
        global $data;
        $data = new stdClass();
        $data->content=$viewName;
        $data->menu = homeMenu();
        require_once("view/mainView.php");
    }

    function loadFilterView($categories) {
        global $data;
        $data = new stdClass();
        $data->categories = $categories;
        $data->content = "filterView.php";
        $data->menu = homeMenu();
        require_once("view/mainView.php");
    }

    function homeMenu() {
        return array(
            "Home" => "index.php",
            'A propos' => "index.php?controller=home&action=aPropos",
            'Voir photos' => "index.php?controller=photo&action=first",
            'Upload' => "index.php?controller=home&action=uploadImage"
        );
    }
    
?>