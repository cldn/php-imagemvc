<?php
    require_once("model/imageDAO.php");

    class PhotoMatrix {

        private $imageDAO;
        
        function __construct() {
            $this->imageDAO = new ImageDAO();
        }

        public function index() {
            $this->first();
        }

        public function first() {
            global $imgId, $size, $nbImg, $category;
            getParam();
            $img = $this->imageDAO->getFirstImage();
            $this->prepareView($category, $img, $size, $nbImg);
        }

        public function next() {
            global $imgId, $size, $nbImg, $category;
            getParam();
            // Make sure we are not going past the image count of the DAO
            if ($imgId + $nbImg < $this->imageDAO->size()) {
                $imgId = $imgId + $nbImg;
            }
            $img = $this->imageDAO->getImage($imgId); 
            $this->prepareView($category, $img, $size, $nbImg);
        }

        public function prev() {
            global $imgId, $size, $nbImg, $category;
            getParam();
            // Make sure we are not going under 0 with previous images
            if ($imgId - $nbImg > 0) {
                $imgId = $imgId - $nbImg;
            } else {
                $imgId = 1;
            }
            $img = $this->imageDAO->getImage($imgId); 
            $this->prepareView($category, $img, $size, $nbImg);
        }

        public function random() {
            // No need to keep the $imgId parameter since we are pulling a random image
            global $size, $nbImg, $category;
            getParam();
            // Get a random image
            if ($category != NULL) {
                $img = $this->imageDAO->getRandomFilteredImage($category);
            } else {
                $img = $this->imageDAO->getRandomImage();
            }
            $this->prepareView($category, $img, $size, $nbImg);
        }

        public function more() {
            global $imgId, $size, $nbImg, $category;
            getParam();
            // Display twice more images
            $nbImg *= 2;
            $img = $this->imageDAO->getImage($imgId); 
            $this->prepareView($category, $img, $size, $nbImg);
        }

        public function less() {
            global $imgId, $size, $nbImg, $category;
            getParam();
            // Display half of the images, not less than 1
            $nbImg = ceil($_GET['nbImg'] / 2);
            $img = $this->imageDAO->getImage($imgId);
            $this->prepareView($category, $img, $size, $nbImg);
        }

        public function zoomin() {
            global $imgId, $size, $nbImg, $category;
            getParam();
            // Zoom in by 25%
            $size *=  1.25;
            $img = $this->imageDAO->getImage($imgId);
            $this->prepareView($category, $img, $size, $nbImg);
        }
        
        public function zoomout() {
            global $imgId, $size, $nbImg, $category;
            getParam();
            // Zoom out by 25%;
            $size *=  0.8;
            $img = $this->imageDAO->getImage($imgId);
            $this->prepareView($category, $img, $size, $nbImg);
        }

        private function prepareView($category, $img, $size, $nbImg) {
            // Prepare the image list from the DAO
            // Build the image matrix with their data and an associated URL
            if ($category == NULL) {
                $imgList = $this->imageDAO->getImageList($img, $nbImg);
            } else {
                $imgList = $this->imageDAO->getFilteredImageList($category, $img, $nbImg);
            }
            foreach ($imgList as $i) {
                $iId = $i->getId();
                $imgMatrixURL[] = array($i->getURL(), "index.php?controller=photo&action=view&imgId=$iId&size=$size");
            }
            
            // Prepare the object to give to the view
            global $data;
            $data = new stdClass();
            if ($category) {
                $data->filter = true;
                $data->categorie = $category;
            }
            $data->content = "photoMatrixView.php";
            $data->size = $size;
            $data->firstId = $img->getId();
            $data->imgMatrix = $imgMatrixURL;
            $data->menu = photoMatrixMenu($img->getId(), $size, $nbImg, $category);
            require_once("view/mainView.php");
        }
    }

    // Get the current parameters : image ID, size of the image(s), number of images, category (for the filter)
    function getParam() {
        global $imgId, $size, $nbImg, $category;
        if (isset($_GET['imgId'])) {
            $imgId = $_GET['imgId'];
        }
        if (isset($_GET['size'])) {
            $size = $_GET['size'];
        } else {
            $size = 480;
        }
        if (isset($_GET['nbImg'])) {
            $nbImg = $_GET['nbImg'];
        } else {
            $nbImg = 2;
        }
        if (isset($_GET['category'])) {
            $category = $_GET['category'];
        } else {
            $category = NULL;
        }
    }

    // Build the menu
    function photoMatrixMenu($imgId, $size, $nbImg, $category) {
        return $category ?
        array(
            "Home" => "index.php",
            'A propos' => "index.php?controller=home&action=aPropos",
            'Upload' => "index.php?controller=home&action=uploadImage",
            'First' => "index.php?controller=photoMatrix&action=first&category=$category",
            'Random' => "index.php?controller=photoMatrix&action=random&size=$size&nbImg=$nbImg&category=$category",
            'Filter' => "index.php?controller=home&action=filter",
            'More' => "index.php?controller=photoMatrix&action=more&size=$size&imgId=$imgId&nbImg=$nbImg&category=$category",
            'Less' => "index.php?controller=photoMatrix&action=less&size=$size&imgId=$imgId&nbImg=$nbImg&category=$category",
            'Zoom +' => "index.php?controller=photoMatrix&action=zoomin&size=$size&imgId=$imgId&nbImg=$nbImg&category=$category",
            'Zoom -' => "index.php?controller=photoMatrix&action=zoomout&size=$size&imgId=$imgId&nbImg=$nbImg&category=$category"
        ) :
        array(
            "Home" => "index.php",
            'A propos' => "index.php?controller=home&action=aPropos",
            'Upload' => "index.php?controller=home&action=uploadImage",
            'First' => "index.php?controller=photoMatrix&action=first&size=$size&nbImg=$nbImg",
            'Random' => "index.php?controller=photoMatrix&action=random&size=$size&nbImg=$nbImg",
            'Filter' => "index.php?controller=home&action=filter",
            'More' => "index.php?controller=photoMatrix&action=more&size=$size&imgId=$imgId&nbImg=$nbImg",
            'Less' => "index.php?controller=photoMatrix&action=less&size=$size&imgId=$imgId&nbImg=$nbImg",
            'Zoom +' => "index.php?controller=photoMatrix&action=zoomin&size=$size&imgId=$imgId&nbImg=$nbImg",
            'Zoom -' => "index.php?controller=photoMatrix&action=zoomout&size=$size&imgId=$imgId&nbImg=$nbImg"
        );
    }

?>